# Machine learning approach to identify phenotypes in patients with ischaemic heart failure with reduced ejection fraction

This public repository contains the source code and the models used in the paper.

01_dataExtraction/* files are not included as they are specific of our datawarehouse.

## Abstract

### Aims
Patients experiencing ischaemic heart failure with reduced ejection fraction (HFrEF) represent a diverse group. We hypothesize that machine learning clustering can help separate distinctive patient phenotypes, paving the way for personalized management.

### Methods and results
A total of 8591 ischaemic HFrEF patients pooled from the EPHESUS and CAPRICORN trials (64 ± 12 years; 28% women) were included in this analysis. Clusters were identified using both clinical and biological variables. Association between clusters and the composite of (i) heart failure hospitalization or all-cause death, (ii) cardiovascular (CV) hospitalization or all-cause death, and (iii) major adverse CV events was assessed. The derived algorithm was applied in the COMMANDER-HF trial (n = 5022) for external validation. Five clinical distinctive clusters were identified: Cluster 1 (n = 2161) with the older patients, higher prevalence of atrial fibrillation and previous CV events; Cluster 2 (n = 1376) with the higher prevalence of older hypertensive women and smoking habit; Cluster 3 (n = 1157) with the higher prevalence of diabetes and peripheral artery disease; Cluster 4 (n = 2073) with relatively younger patients, mostly men and with the higher left ventricular ejection fraction; Cluster 5 (n = 1824) with the younger patients and lower CV events burden. Cluster membership was efficiently predicted by a random forest algorithm. Clusters were significantly associated with outcomes in derivation and validation datasets, with Cluster 1 having the highest risk, and Cluster 4 the lowest. Mineralocorticoid receptor antagonist benefit on CV hospitalization or all-cause death was magnified in clusters with the lowest risk of events (Clusters 2 and 4).

### Conclusion
Clustering reveals distinct risk subgroups in the heterogeneous array of ischaemic HFrEF patients. This classification, accessible online, could enhance future outcome predictions for ischaemic HFrEF cases.


## Requirement
  - python==3.10
    - lifelines==0.26.5
    - matplolib==3.5.1
    - pandas==1.3.5
    - python-javabridge==4.0.3
    - python-weka-wrapper3==0.2.9
  - R 4.3.1
    - dplyr
    - fmsb
    - forestmodel
    - gtsummary
    - knitr
    - readr
    - survival
    - survminer
    - tidyverse
    - VarSelLCM
