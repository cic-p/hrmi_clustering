#! /usr/bin/env python3

import argparse

import pandas as pd
from weka.classifiers import Classifier
from weka.core import jvm
from weka.core.converters import Loader
from weka.filters import Filter


def main(inpt_file: str, separator: str, missing: str, inpt_model: str):

    # start the jvm
    jvm.start()

    # load data
    loader = Loader(
        classname="weka.core.converters.CSVLoader",
        options=["-M", missing, "-F", separator],
    )
    data = loader.load_file(inpt_file)

    # add cluster column for storing prediction
    add_cluster = Filter(
        classname="weka.filters.unsupervised.attribute.Add",
        options=["-T", "NOM", "-N", "cluster", "-L", "1,2,3,4,5", "-C", "last"],
    )
    add_cluster.inputformat(data)
    data = add_cluster.filter(data)
    data.class_is_last()

    # load model
    cls = Classifier(
        classname="weka.classifiers.misc.InputMappedClassifier",
        options=["-I", "-L", inpt_model, "-M"],
    )

    # predict!
    predictions = []
    for index, inst in enumerate(data):
        pred = cls.classify_instance(inst)
        predictions.append(inst.dataset.class_attribute.value(int(pred)))

    results = pd.read_csv(inpt_file, low_memory=False, sep=separator)
    results["predicted"] = predictions
    results.to_csv(f"{inpt_file}.predicted.csv", sep=separator)

    # stop the jvm
    jvm.stop()


if __name__ == "__main__":
    arguments = argparse.ArgumentParser()
    arguments.add_argument(
        "-i",
        "--input",
        metavar="csv file",
        required=True,
        type=str,
        help="input data file",
    )
    arguments.add_argument(
        "-s",
        "--separator",
        metavar="separator",
        type=str,
        default=",",
        help="csv sepatator",
    )
    arguments.add_argument(
        "-ms",
        "--missing",
        metavar="missing values",
        required=True,
        type=str,
        help="missing values code",
    )
    arguments.add_argument(
        "-m",
        "--model",
        metavar="model",
        required=True,
        type=str,
        help="Weka model file",
    )

    parameters_arguments = arguments.parse_args()
    inpt_file = parameters_arguments.input
    separator = parameters_arguments.separator
    missing = parameters_arguments.missing
    model = parameters_arguments.model

    # Call to the main function
    main(inpt_file, separator, missing, model)
